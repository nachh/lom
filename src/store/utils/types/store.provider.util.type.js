// @flow

export type StoreProviderUtilType = {
  initialState: Object,
  reducers: Object,
};
