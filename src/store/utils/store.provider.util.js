// @flow

// Vendors
import { createStore, combineReducers, compose, applyMiddleware } from "redux";
// Middlewares
import middlewares from "./config/store-middlewares.provider.util";
// Types
import type { StoreProviderUtilType } from "./types/store.provider.util.type";

const configureStore = ({
  reducers,
  initialState,
}: StoreProviderUtilType): any =>
  createStore(
    combineReducers(reducers),
    initialState,
    compose(
      applyMiddleware(...middlewares),
      typeof window === "object" && window.__REDUX_DEVTOOLS_EXTENSION__
        ? window.__REDUX_DEVTOOLS_EXTENSION__()
        : (f) => f
    )
  );

export default configureStore;
