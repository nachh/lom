// @flow

// Vendors
import React from "react";
import { Provider } from "react-redux";
// Constants
import constants from "./constants/store.provider.constants";
// Reducers
import reducers from "./reducers/store.provider.reducers";
// Utils
import configureStore from "./utils/store.provider.util";

const StoreProvider = ({ children }: { children: React$Node }): React$Node => {
  // Configure the store
  const store = configureStore({
    reducers,
    initialState: constants.INITIAL_STATE,
  });

  return <Provider store={store}>{children}</Provider>;
};

export default StoreProvider;
