// @flow

export type StoreQuizProviderReducerType = {
  correct: boolean,
  id: number,
};
