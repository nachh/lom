// @flow

// Constants
import constants from "../constants/store-quiz.provider.reducer.constants";
// Types
import type { StoreQuizProviderReducerType } from "../types/store-quiz.provider.reducer.type";

const setQuiz = (quiz: StoreQuizProviderReducerType): Object => ({
  type: constants.SET_QUIZ,
  payload: quiz,
});

const setPosition = (position: number): Object => ({
  type: constants.SET_POSITION,
  payload: position,
});

export { setQuiz, setPosition };
