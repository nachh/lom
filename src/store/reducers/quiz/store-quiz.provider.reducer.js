// @flow

// Constants
import constants from "./constants/store-quiz.provider.reducer.constants";
// Types
import type { StoreActionProviderReducerType } from "../types/store-action.provider.reducer.type";
import type { StoreStateProviderReducerType } from "../types/store-state.provider.reducer.type";

export default function QuizReducer(
  state: StoreStateProviderReducerType = {},
  action: StoreActionProviderReducerType
): StoreStateProviderReducerType {
  const newState = { ...state };
  switch (action?.type) {
    case constants.SET_QUIZ:
      newState.questions.push(action?.payload);
      break;
    case constants.SET_POSITION:
      newState.position = action?.payload;
      break;
  }
  return newState;
}
