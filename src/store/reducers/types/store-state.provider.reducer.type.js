// @flow

// Types
import type { StoreQuizProviderReducerType } from "../quiz/types/store-quiz.provider.reducer.type";

export type StoreStateProviderReducerType = {
  quiz: StoreQuizProviderReducerType,
  questions: Array<mixed>,
  position: number,
};
