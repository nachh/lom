// @flow

export type StoreActionProviderReducerType = {
  type: string,
  payload: Object | number,
  position: number,
};
