// @flow
import * as React from "react";
// views
import QuizView from "./views/quiz/quiz.view";
import HomeView from "./views/home/home.view";
import SummaryView from "./views/summary/summary.view";
// Styles
import "./App.css";
import "rsuite/dist/rsuite.min.css";
// Vendors
import { CustomProvider } from "rsuite";
// Router
import { HashRouter, Route, Routes } from "react-router-dom";

function App(): React.Node {
  return (
    <CustomProvider>
      <HashRouter forceRefresh={true}>
        <Routes>
          <Route exact index path="/" element={<HomeView />} />
          <Route exact path="/quiz" element={<QuizView />} />
          <Route exact path="/summary" element={<SummaryView />} />
        </Routes>
      </HashRouter>
    </CustomProvider>
  );
}

export default App;
