// @flow

import * as React from "react";
// Router
import { useNavigate } from "react-router-dom";
// Styled
import {
  WrapperStyled,
  TextWrapperStyled,
  ContentWrapperStyled,
  ButtonStyled,
} from "./home.view.styled";

const HomeView = (): React.Node => {
  const navigate = useNavigate();

  return (
    <WrapperStyled>
      <ContentWrapperStyled>
        <TextWrapperStyled>
          <p>
            It&apos;s the day. The day when LOM gives humanity a chance to save
            itself, to avoid its horrible fate.
          </p>
          <p>
            The day when by passing this test mankind could get the chance to
            travel back in time... to the moment that changed everything...
          </p>
          <p>Here you are, your destiny, our destiny is in your hands...</p>
          All you have to do is pass this ten-question quiz about the wizarding
          world of Harry Potter.
          <p>good luck.</p>
        </TextWrapperStyled>
        <ButtonStyled
          onClick={() => navigate("/quiz", { replace: true })}
          appearance="ghost"
        >
          START
        </ButtonStyled>
      </ContentWrapperStyled>
    </WrapperStyled>
  );
};

export default HomeView;
