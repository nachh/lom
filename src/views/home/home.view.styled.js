import styled from "styled-components";
// Components
import { Button } from "rsuite";

export const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-width: 600px;
`;
WrapperStyled.displayName = "WrapperStyled";

export const TextWrapperStyled = styled.div`
  text-align: center;
`;
TextWrapperStyled.displayName = "TextWrapperStyled";

export const ContentWrapperStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
`;
ContentWrapperStyled.displayName = "ContentWrapperStyled";

export const ButtonStyled = styled(Button)`
  border-radius: 0;
  margin-top: 40px;
  color: black;
  border-color: black;
  &:hover {
    background-color: black;
    border-color: black;
    color: white;
  }
`;
ButtonStyled.displayName = "ButtonStyled";
