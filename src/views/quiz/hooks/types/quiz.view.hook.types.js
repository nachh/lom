// @flow

export type quizHookReturnType = {
  position: number,
  handleStepStatus: Function,
};
