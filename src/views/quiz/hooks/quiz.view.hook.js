// @flow

// Redux
import { useSelector } from "react-redux";

// Handlers
import quizHandlers from "../handlers/quiz.view.handler";
// Types
import type { quizHookReturnType } from "./types/quiz.view.hook.types";

const useQuizHook = (): quizHookReturnType => {
  const position = useSelector((data) => data.data.position);
  const questions = useSelector((data) => data.data.questions);

  const { handleStepStatus } = quizHandlers({ questions });

  return { position, handleStepStatus };
};

export default useQuizHook;
