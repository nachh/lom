// @flow

export type quizHandlersType = {
  questions: any,
};
export type quizHandlersReturnType = {
  handleStepStatus: Function,
};
