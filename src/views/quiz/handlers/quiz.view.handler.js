// @flow

// Types
import type {
  quizHandlersType,
  quizHandlersReturnType,
} from "./types/quiz.view.handler.types";

const stepStatusHandler = (i, questions) =>
  ({
    ["true"]: "finish",
    ["false"]: "error",
  }[questions[i]?.correct.toString()]);

const quizHandlers = ({
  questions,
}: quizHandlersType): quizHandlersReturnType => ({
  handleStepStatus: ({ i }) => stepStatusHandler(i, questions),
});

export default quizHandlers;
