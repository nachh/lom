export default {
  QUESTION_1: {
    correctAnswerId: 4,
    options: [
      "Chudley Cannons",
      "Holyhead Harpies",
      "Caerphilly Catapults",
      "Puddlemore United",
    ],
    questionId: 1,
    questionText:
      "What was the name of the Quidditch team on the sock Mrs. Weasley tries to give to Harry?",
  },
  QUESTION_2: {
    correctAnswerId: 2,
    options: [
      "Six",
      "Eight in total, including Harry Potter himself",
      "Seven in total, including Harry Potter himself",
      "Seven in total, including Stan Shunpike himself",
    ],
    questionId: 2,
    questionText: "How many Horcruxes were made?",
  },
  QUESTION_3: {
    correctAnswerId: 3,
    options: ["400", "500", "700", "800"],
    questionId: 3,
    questionText:
      "In the game of Quidditch, how many possible fouls are there?",
  },
  QUESTION_4: {
    correctAnswerId: 2,
    options: [
      "Hannah Abbott",
      "Reginald Cattermole",
      "Stan Shunpike",
      "Cornelius Fudge",
    ],
    questionId: 4,
    questionText:
      "Which character did Ron impersonate to steal Slytherin`s locket?",
  },
  QUESTION_5: {
    correctAnswerId: 3,
    options: [
      "Sirius Black",
      "Bellatrix Lestrange",
      "Barty Crouch Jr",
      "Albus Dumbledore",
    ],
    questionId: 5,
    questionText: "Who was the first prisoner to escape from Azkaban?",
  },
  QUESTION_6: {
    correctAnswerId: 1,
    options: ["6-2-4-4-2", "6-1-4-4-2", "6-2-3-3-2", "6-2-4-1-2"],
    questionId: 6,
    questionText:
      "What is the five digit code that one needs to dial to get inside the Ministry of Magic?",
  },
  QUESTION_7: {
    correctAnswerId: 1,
    options: ["Hufflepuff", "Gryffindor", "Ravenclaw", "Slytherin"],
    questionId: 7,
    questionText: "Hannah Abbott is sorted into which house?",
  },
  QUESTION_8: {
    correctAnswerId: 4,
    options: [
      "Severus Snape",
      "Mykew Gregorovitch",
      "Lord Voldemort",
      "Garrick Ollivander",
    ],
    questionId: 8,
    questionText: "Who said, 'The wand chooses the wizard, Mr. Potter.'",
  },
  QUESTION_9: {
    correctAnswerId: 3,
    options: ["Row 77", "Row 57", "Row 97", "Row 87"],
    questionId: 9,
    questionText:
      "Which row houses the prophecy about Harry and Lord Voldemort in the House of Prophecy?",
  },
  QUESTION_10: {
    correctAnswerId: 2,
    options: [
      "A perfect miniature of a London Bus",
      "A perfect map of the London underground",
      "A perfect portrait of Hagrid's face",
      "What scar?",
    ],
    questionId: 10,
    questionText:
      "What does the scar above Albus Dumbledore's left knee look like?",
  },
};
