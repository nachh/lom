// @flow

import * as React from "react";
// Components
import Question from "./components/question/question.component";
// Constants
import constants from "../quiz/constants/quiz.constants";
// Hooks
import useQuizHook from "./hooks/quiz.view.hook";
// Styled
import {
  WrapperStyled,
  QuestionStyled,
  StepsWrapperStyled,
  StepsStyled,
} from "./quiz.view.styled";

const QuizView = (): React.Node => {
  const { position, handleStepStatus } = useQuizHook();

  return (
    <WrapperStyled>
      <StepsWrapperStyled small current={position}>
        {Object.keys(constants).map((keyName, i) => (
          <StepsStyled.Item key={i} status={handleStepStatus({ i })} />
        ))}
      </StepsWrapperStyled>
      <QuestionStyled>
        {Object.keys(constants).map((keyName, i) => (
          <Question
            key={i}
            options={constants[keyName].options}
            questionId={i + 1}
            questionText={constants[keyName].questionText}
            correctAnswerId={constants[keyName].correctAnswerId}
          />
        ))}
      </QuestionStyled>
    </WrapperStyled>
  );
};

export default QuizView;
