import styled from "styled-components";
import { Steps } from "rsuite";

export const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;
WrapperStyled.displayName = "WrapperStyled";

export const QuestionStyled = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    max-width: 800px;
    min-width: 800px;
    max-height: 300px;
    min-height: 300px;
    height: 300px;
    border: 1px solid #212121;
    padding: 20px;
    text-align: center;
    & h3 {
      text-align: center;
  `;
QuestionStyled.displayName = "QuestionStyled";

export const StepsStyled = styled(Steps)``;
StepsStyled.displayName = "StepsStyled";

export const StepsWrapperStyled = styled(Steps)`
  .rs-steps-item-icon {
    color: transparent;
    border-color: #212121;
    font-size: 0;
  }
  .rs-steps-item-active .rs-steps-item-icon-wrapper {
    background-color: transparent;
    border-color: #212121;
  }
  .rs-steps-item-status-finish .rs-steps-item-icon-wrapper {
    border-color: #66bb6a;
    background: #66bb6a;
  }

  .rs-steps-item-status-error .rs-steps-item-icon-wrapper {
    border-color: #ef5350;
    background: #ef5350;
  }
  .rs-steps-item-icon-undefined {
  }
`;
StepsWrapperStyled.displayName = "StepsWrapperStyled";
