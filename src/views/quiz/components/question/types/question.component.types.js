// @flow

export type questionType = {
  questionId: number,
  questionText: string,
  options: any,
  correctAnswerId: number,
};
