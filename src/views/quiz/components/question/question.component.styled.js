import styled from "styled-components";
import { Button, RadioGroup, Radio } from "rsuite";

export const ButtonStyled = styled(Button)`
  border-radius: 0;

${(props) =>
  props.disabled == true
    ? `
        color: grey !important;
        border-color: grey;
        &:hover{
          color: grey;
          border-color: grey;
        }
      `
    : `
        color: black;
        border-color: black;
        &:hover{
          background-color: black;
          border-color: black;
          color: white;
        }
      `}
  }
`;
ButtonStyled.displayName = "ButtonStyled";

export const QuestionWrapperStyled = styled.div`
  display: ${(props) => (props.show ? "inline" : "none")};
`;
QuestionWrapperStyled.displayName = "QuestionWrapperStyled";

export const RadioWrapperStyled = styled.div`
  display: flex;
  justify-content: center;
  padding: 50px 0;
  text-align-left;
`;
RadioWrapperStyled.displayName = "RadioWrapperStyled";

export const RadioGroupStyled = styled(RadioGroup)`
  text-align: left;
  ${({ status, showcorrect }) =>
    status === "error" &&
    showcorrect === "true" &&
    `.show-correct{
      color: limegreen;
      label{
        text-decoration: underline
      }
    }
  `}
`;
RadioGroupStyled.displayName = "RadioGroupStyled";

export const RadioStyled = styled(Radio)`
  &[aria-checked="true"] {
    color: ${(props) => (props.status === "correct" ? "#66bb6a" : "#ef5350")};
  }
  &.rs-radio-disabled > .rs-radio-checker > label {
    color: inherit;
  }
`;
RadioStyled.displayName = "RadioStyled";

export const TittleWrapperStyled = styled.div`
  display: flex;
  justify-content: center;
`;
TittleWrapperStyled.displayName = "TittleWrapperStyled";
export const TitleStyled = styled.h4`
  // max-width: 60%;
`;
TitleStyled.displayName = "TitleStyled";
