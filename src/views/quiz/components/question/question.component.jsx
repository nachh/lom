// @flow

// Vendors
import React from "react";
// Hooks
import useQuestionHook from "./hooks/question.component.hook";
// Types
import type { questionType } from "./types/question.component.types";
// Styled
import {
  ButtonStyled,
  QuestionWrapperStyled,
  RadioGroupStyled,
  RadioStyled,
  RadioWrapperStyled,
  TitleStyled,
  TittleWrapperStyled,
} from "./question.component.styled";

const Question = ({
  options,
  questionId,
  questionText,
  correctAnswerId,
}: questionType): any => {
  const { handleOnClick, handleOnChange, show, correct, showCorrect } =
    useQuestionHook({
      questionId,
    });

  return (
    <QuestionWrapperStyled show={show}>
      <TittleWrapperStyled>
        <TitleStyled>{questionText}</TitleStyled>
      </TittleWrapperStyled>
      <RadioWrapperStyled>
        <RadioGroupStyled
          inline
          name="radioList"
          onChange={(str, e) =>
            handleOnChange(str, e.target.attributes.answerid.value)
          }
          status={correct ? "correct" : "error"}
          showcorrect={showCorrect ? "true" : "false"}
        >
          {options.map((option, index) => (
            <RadioStyled
              key={index}
              value={index + 1}
              inputProps={{ answerid: index + 1 }}
              status={correctAnswerId === index + 1 ? "correct" : "error"}
              className={correctAnswerId === index + 1 ? "show-correct" : ""}
              disabled={showCorrect}
            >
              {option}
            </RadioStyled>
          ))}
        </RadioGroupStyled>
      </RadioWrapperStyled>
      <ButtonStyled
        appearance="ghost"
        disabled={!showCorrect ? true : false}
        onClick={handleOnClick}
      >
        CONTINUE
      </ButtonStyled>
    </QuestionWrapperStyled>
  );
};

export default Question;
