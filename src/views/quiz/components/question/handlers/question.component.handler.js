// @flow

// Utils
import validateAnswer from "../utils/question.component.util";
// Types
import type {
  questionHandlersType,
  questionHandlersReturnType,
} from "./types/question.component.handler.types";
import constants from "../../../constants/quiz.constants";
// Redux
import {
  setQuiz,
  setPosition,
} from "../../../../../store/reducers/quiz/actions/store-quiz.provider.reducer.actions";

const onChangeHandler = ({
  answerId,
  questionId,
  setCorrect,
  setValue,
  value,
  setShowCorrect,
}) => {
  setValue(value);
  if (validateAnswer({ answerId, questionId })) {
    setCorrect(true);
  } else {
    setCorrect(false);
  }
  setShowCorrect(true);
};

const onClickHandler = ({
  correct,
  dispatch,
  position,
  questionId,
  constants,
  navigate,
}: {
  correct: boolean,
  dispatch: Function,
  position: number,
  questionId: number,
  navigate: Function,
  constants: any,
}) => {
  dispatch(
    setQuiz({
      id: questionId,
      correct: correct,
    })
  );
  dispatch(setPosition(position + 1));
  if (position == Object.keys(constants).length - 1) {
    navigate("/summary", { replace: true });
  }
};

const questionHandlers = ({
  questionId,
  setCorrect,
  setValue,
  correct,
  dispatch,
  position,
  setShowCorrect,
  navigate,
}: questionHandlersType): questionHandlersReturnType => ({
  handleOnChange: (answerId: number, value: string) =>
    onChangeHandler({
      answerId,
      questionId,
      setCorrect,
      setValue,
      value,
      setShowCorrect,
    }),
  handleOnClick: () =>
    onClickHandler({
      correct,
      dispatch,
      position,
      questionId,
      constants,
      navigate,
    }),
});

export default questionHandlers;
