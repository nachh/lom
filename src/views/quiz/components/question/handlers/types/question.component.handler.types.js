// @flow

export type questionHandlersType = {
  questionId: number,
  setCorrect: Function,
  setPosition: Function,
  setValue: Function,
  navigate: Function,
  position: number,
  dispatch: Function,
  correct: boolean,
  setShowCorrect: Function,
};

export type questionHandlersReturnType = {
  handleOnChange: Function,
  handleOnClick: Function,
};
