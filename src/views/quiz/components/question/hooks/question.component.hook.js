// @flow

import React from "react";
// Redux
import { useDispatch, useSelector } from "react-redux";
import {
  setQuiz,
  setPosition,
} from "../../../../../store/reducers/quiz/actions/store-quiz.provider.reducer.actions";
// Handlers
import questionHandlers from "../handlers/question.component.handler";
// Types
import type {
  questionHookType,
  questionHookReturnType,
} from "./types/question.component.hook.types";
// Router
import { useNavigate } from "react-router-dom";

const useQuestionHook = ({
  questionId,
}: questionHookType): questionHookReturnType => {
  const [value, setValue] = React.useState(null);
  const [correct, setCorrect] = React.useState(false);
  const [show, setShow] = React.useState(false);
  const [showCorrect, setShowCorrect] = React.useState(false);
  const position = useSelector((data) => data.data.position);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  React.useEffect(() => {
    if (position === questionId - 1) {
      setShow(true);
    } else {
      setShow(false);
    }
  }, [position, questionId]);

  const { handleOnChange, handleOnClick } = questionHandlers({
    correct,
    questionId,
    setCorrect,
    setValue,
    setShow,
    setQuiz,
    setPosition,
    dispatch,
    position,
    setShowCorrect,
    navigate,
  });

  return {
    correct,
    handleOnChange,
    handleOnClick,
    position,
    show,
    value,
    showCorrect,
  };
};

export default useQuestionHook;
