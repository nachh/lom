// @flow

export type questionHookType = {
  questionId: number,
};

export type questionHookReturnType = {
  correct: boolean,
  handleOnChange: Function,
  showCorrect: boolean,
  show: boolean,
  handleOnClick: Function,
};
