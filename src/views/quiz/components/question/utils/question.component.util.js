// @flow

// Constants
import constants from "../../../constants/quiz.constants";
// Types
import type { questionUtilType } from "./types/question.component.util.type";

const validateAnswer = ({ answerId, questionId }: questionUtilType): any =>
  ({
    [constants.QUESTION_1.questionId]:
      answerId === constants.QUESTION_1.correctAnswerId ? true : false,
    [constants.QUESTION_2.questionId]:
      answerId === constants.QUESTION_2.correctAnswerId ? true : false,
    [constants.QUESTION_3.questionId]:
      answerId === constants.QUESTION_3.correctAnswerId ? true : false,
    [constants.QUESTION_4.questionId]:
      answerId === constants.QUESTION_4.correctAnswerId ? true : false,
    [constants.QUESTION_5.questionId]:
      answerId === constants.QUESTION_5.correctAnswerId ? true : false,
    [constants.QUESTION_6.questionId]:
      answerId === constants.QUESTION_6.correctAnswerId ? true : false,
    [constants.QUESTION_7.questionId]:
      answerId === constants.QUESTION_7.correctAnswerId ? true : false,
    [constants.QUESTION_8.questionId]:
      answerId === constants.QUESTION_8.correctAnswerId ? true : false,
    [constants.QUESTION_9.questionId]:
      answerId === constants.QUESTION_9.correctAnswerId ? true : false,
    [constants.QUESTION_10.questionId]:
      answerId === constants.QUESTION_10.correctAnswerId ? true : false,
  }[questionId]);

export default validateAnswer;
