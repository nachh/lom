// @flow

export type questionUtilType = {
  answerId: number,
  questionId: number,
};
