// @flow

export type summaryHookReturnType = {
  counter: number,
  setMessage: Function,
};
