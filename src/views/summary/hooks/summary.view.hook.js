// @flow

// Redux
import { useSelector } from "react-redux";
// Types
import type { summaryHookReturnType } from "./types/summary.view.hook.types";

const useSummaryHook = (): summaryHookReturnType => {
  const questions = useSelector((data) => data.data.questions);
  let counter = 0;

  questions.map((question) => {
    if (question.correct) {
      counter = counter + 1;
    }
  });

  const setMessage = () =>
    ({
      "0": "Pathetic.. Avada Kedavra!",
      "1": "Insulting.. Avada Kedavra!",
      "2": "Poor.. Avada Kedavra!",
      "3": "Don't waste my time.. Avada Kedavra!",
      "4": "Not bad.. Avada Kedavra!",
      "5": "There is room for improvement.. Avada Kedavra!",
      "6": "Better than many, but not enough.. Avada Kedavra!",
      "7": "A great result.. Avada Kedavra!",
      "8": "Brilliant ! But not enough.. Avada Kedavra!",
      "9": "Close to perfection, but just that, close.. Avada Kedavra!",
      "10": "CONGRATULATIONS! You saved the mankind. Today.",
    }[counter]);

  return { counter, setMessage };
};

export default useSummaryHook;
