import styled from "styled-components";

export const WrapperStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-width: 600px;
`;
WrapperStyled.displayName = "WrapperStyled";

export const ContentWrapperStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  height: 100vh;
`;
ContentWrapperStyled.displayName = "ContentWrapperStyled";
