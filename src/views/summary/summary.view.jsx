// @flow

import * as React from "react";
// Hooks
import useSummaryHook from "../summary/hooks/summary.view.hook";
// Styled
import { WrapperStyled, ContentWrapperStyled } from "./summary.view.styled";

const SummaryView = (): React.Node => {
  const { counter, setMessage } = useSummaryHook();

  return (
    <WrapperStyled>
      <ContentWrapperStyled>
        <h3>Correct answers: {counter} of 10</h3>
        {setMessage()}
      </ContentWrapperStyled>
    </WrapperStyled>
  );
};

export default SummaryView;
