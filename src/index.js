// @flow
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// Vendors
import StoreProvider from "./store/store.provider";
// Styles
import "./index.css";

ReactDOM.render(
  <StoreProvider>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </StoreProvider>,

  document.getElementById("root")
);
